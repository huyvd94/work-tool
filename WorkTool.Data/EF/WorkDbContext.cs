﻿using Microsoft.EntityFrameworkCore;
using WorkTool.Data.Models;

namespace WorkTool.Data.EF
{
    public class WorkDbContext : DbContext
    {
        public WorkDbContext(DbContextOptions options): base(options)
        {

        }

        DbSet<Project> Projects { get; set; }
    }
}
